package denis.kolpashikov.ru.motivator.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import denis.kolpashikov.ru.motivator.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoTaskFragment extends Fragment {


    public NoTaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_no_task, null);
    }

}
