package denis.kolpashikov.ru.motivator.CommonClasses;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import denis.kolpashikov.ru.motivator.MainActivity;
import denis.kolpashikov.ru.motivator.R;

public class NewTaskDialog extends DialogFragment implements View.OnClickListener {
    private final String LOGS = "mLogs";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
    private EditText etTaskName;
    private EditText etStartDate;
    private EditText etEndDate;
    private AppCompatActivity activity = null;
    private ActivityClickListener mListener = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Date date = new Date();

        View view;
        view = inflater.inflate(R.layout.date_time_item, null);
        etTaskName = (EditText)view.findViewById(R.id.et_task_name);
        etTaskName.requestFocus();
//        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        imm.showSoftInput(etTaskName, InputMethodManager.SHOW_FORCED);

        etStartDate = (EditText)view.findViewById(R.id.et_start_date);
        etStartDate.setText(sdf.format(date));
        etEndDate = (EditText)view.findViewById(R.id.et_end_date);
        etEndDate.setText(sdf.format(date));
        view.findViewById(R.id.btn_ok).setOnClickListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);

        if(getDialog() != null){
            getDialog().setCanceledOnTouchOutside(false);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_ok:
                if(mListener != null){
                    String stTaskName = etTaskName.getText().toString();
                    String s = etStartDate.getText().toString();
                    Date datestart = null;
                    try {
                        datestart = sdf.parse(s);
                    }catch(ParseException e){
                        e.printStackTrace();
                    }

                    Date dateend = null;
                    s = etEndDate.getText().toString();
                    try{
                        dateend = sdf.parse(s);
                    }catch(ParseException e){
                        e.printStackTrace();
                    }

                    MTask newTask = new MTask(stTaskName, datestart, dateend, false, false);
                    mListener.addToDatabase(newTask);
                    dismiss();
                }
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activity = (MainActivity) context;
            mListener = (ActivityClickListener) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implements ActivityClickListener");
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }


}
