package denis.kolpashikov.ru.motivator.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import denis.kolpashikov.ru.motivator.CommonClasses.MTask;
import denis.kolpashikov.ru.motivator.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    private String name;
    private Date dateStart;
    private Date dateEnd;
    private boolean isdone;
    private boolean isstart;
    private SimpleDateFormat sdf;
    private MTask m_CurrentTask;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_screen, null);
        MTask task = getTask();


        return view;
    }

    private MTask getTask(){
        Bundle bundle = this.getArguments();
        sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        name = bundle.getString("name");

        String ds = bundle.getString("datestart");
        try {
            dateStart = sdf.parse(ds);
        } catch(ParseException e){
            e.printStackTrace();
        }

        String de = bundle.getString("dateend");
        try{
            dateEnd = sdf.parse(de);
        }catch(ParseException e){
            e.printStackTrace();
        }

        isdone = bundle.getBoolean("isdone");
        isstart = bundle.getBoolean("isstart");
        return new MTask(name, dateStart, dateEnd, isdone, isstart);
    }
    
}
