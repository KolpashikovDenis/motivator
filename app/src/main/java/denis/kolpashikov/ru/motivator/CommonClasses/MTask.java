package denis.kolpashikov.ru.motivator.CommonClasses;

import java.util.Date;

public class MTask {
    public String name;
    public Date dateStart;
    public Date dateEnd;
    public boolean isDone;
    public boolean isStart;

    public MTask() {
    }

    public MTask(String name, Date dateStart, Date dateEnd, boolean isDone, boolean isStart) {
        this.name = name;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.isDone = isDone;
        this.isStart = isStart;
    }
}
