package denis.kolpashikov.ru.motivator;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import denis.kolpashikov.ru.motivator.Adapters.TaskAdapter;
import denis.kolpashikov.ru.motivator.CommonClasses.ActivityClickListener;
import denis.kolpashikov.ru.motivator.CommonClasses.NewTaskDialog;
import denis.kolpashikov.ru.motivator.CommonClasses.MTask;
import denis.kolpashikov.ru.motivator.Fragments.MainFragment;
import denis.kolpashikov.ru.motivator.Fragments.NoPermissionsFragment;
import denis.kolpashikov.ru.motivator.Fragments.NoTaskFragment;

public class MainActivity extends AppCompatActivity implements ActivityClickListener {
    // 0. Так, некоторые переменные
    private int REQUEST_PERMISSIONS = 1;
    private final String LOGS = "Logs";
    public String googleAccount = "";
    private StringBuilder logMsg;
    private int DIALOG_ID = 1;
    private String preparedAccountName;
    private String updateKeys;

    // 1. Данные
    private ArrayList<MTask> taskList;
    private MTask mCurrentTask;

    // 2. Интерфейс
    private TaskAdapter taskAdapter;
    private ListView taskListView;
    private Button btnAddTask;
    private LinearLayout llnearLayout;

    private Fragment fragmentMain;
    private Fragment fragmentNoTask;
    private Fragment fragmentNoPermissions;
    private FragmentTransaction ft;

    // 3. Firebase
    private FirebaseDatabase m_FirebaseDatabase;
    private DatabaseReference m_dbCurrentTask;
    private ChildEventListener m_ChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentMain = new MainFragment();
        fragmentNoPermissions = new NoPermissionsFragment();
        fragmentNoTask = new NoTaskFragment();


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            AccountManager am = AccountManager.get(this);
            Account[] accounts = am.getAccountsByType("com.google");
            googleAccount = accounts[0].name;
            showFirstStartScreen();
            tryReadDatabase();
        } else {
            showEmptyScreen();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS)
                    == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_PERMISSIONS);
                }

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_PERMISSIONS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AccountManager am = AccountManager.get(this);
                Account[] accounts = am.getAccountsByType("com.google");
                googleAccount = accounts[0].name;
                showFirstStartScreen();

                // Здесь начинается работа с Firebase Realtime Database
                tryReadDatabase();
            } else {
                showEmptyScreen();
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private void showMainScreen(MTask mCurrentTask) {
        fillCurrentTaskLayout(mCurrentTask);
    }

    private void fillCurrentTaskLayout(final MTask task) {
        Bundle bundle = new Bundle();
        bundle.putString("name", task.name);
        bundle.putString("datestart", task.dateStart.toString());
        bundle.putString("dateend", task.dateEnd.toString());
        bundle.putBoolean("isdone", task.isDone);
        bundle.putBoolean("isstart", task.isStart);
        fragmentMain.setArguments(bundle);
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragmentMain).commit();

//        if(task.isDone){
//            btnTaskDone.setVisibility(View.GONE);
//        }else{
//            btnTaskDone.setVisibility(View.VISIBLE);
//        }
//        btnTaskDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                btnTaskDone.setVisibility(View.GONE);
//                Map updatesChildren = new HashMap<String, Object>();
//                updatesChildren.put("name", task.name);
//                updatesChildren.put("dateStart", task.dateStart);
//                updatesChildren.put("dateEnd", task.dateEnd);
//                updatesChildren.put("isDone", true);
//                updatesChildren.put("isStart", true);
//                DatabaseReference m_dbUpdates = m_FirebaseDatabase.getReference()
//                        .child("Users").child(preparedAccountName).child("current_task").child(updateKeys);
//                m_dbUpdates.updateChildren(updatesChildren);
//            }
//        });
    }

    private void showFirstStartScreen() {
        ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, fragmentNoTask).commit();
    }

    private void showEmptyScreen() {
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragmentNoPermissions).commit();
    }

    private void tryReadDatabase() {
        if (!(googleAccount.length() > 0)) {
            return;
        }

        preparedAccountName = prepareAccountName(googleAccount);
        m_FirebaseDatabase = FirebaseDatabase.getInstance();
        m_dbCurrentTask = m_FirebaseDatabase.getReference().child("Users").child(preparedAccountName).child("current_task");

        if(m_ChildEventListener == null){
            m_ChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    MTask task = dataSnapshot.getValue(MTask.class);
                    updateKeys = dataSnapshot.getKey();
                    showMainScreen(task);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
        }
        m_dbCurrentTask.addChildEventListener(m_ChildEventListener);

        btnAddTask = (Button) findViewById(R.id.btn_addtask);
        btnAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewTaskDialog dlg = new NewTaskDialog();
                dlg.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    private String prepareAccountName(final String accountName) {
        StringBuilder localSB = new StringBuilder();
        // Firebase Database paths must not contain '.', '#', '$', '[', or ']'
        char[] arr = accountName.toCharArray();
        for (char c : arr) {
            if (!(c == '.' || c == '#' || c == '$' || c == '[' || c == ']' || c == '@')) {
                localSB.append(c);
            }
        }
        return localSB.toString();
    }

    @Override
    public void addToDatabase(MTask task) {
        if(m_dbCurrentTask != null){
            m_dbCurrentTask.push().setValue(task, new DatabaseReference.CompletionListener(){
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Toast.makeText(getApplicationContext(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        ((Button)findViewById(R.id.button_done)).setVisibility(View.VISIBLE);
                    }

                }
            });
        }
    }
}
