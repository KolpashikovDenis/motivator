package denis.kolpashikov.ru.motivator.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import denis.kolpashikov.ru.motivator.CommonClasses.MTask;
import denis.kolpashikov.ru.motivator.R;

public class TaskAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<MTask> taskList;

    public TaskAdapter(Context _context, ArrayList<MTask> _taskList) {
        super();
        context = _context;
        taskList = _taskList;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Object getItem(int position) {
        return taskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = inflater.inflate(R.layout.item_task, parent, false);
        }
        MTask task = getTask(position);
        ((TextView)view.findViewById(R.id.name_textview)).setText(task.name);
        ((TextView)view.findViewById(R.id.date_start_textview)).setText(task.dateStart.toString());
        ((TextView)view.findViewById(R.id.date_end_textview)).setText(task.dateEnd.toString());
        Button button = view.findViewById(R.id.button_done);
        button.setVisibility(View.GONE);

        return view;
    }

    private MTask getTask(int pos){
        return (MTask)getItem(pos);
    }
}
